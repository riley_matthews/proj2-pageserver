from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/')
def temp():
	# we are going to forbid users from accessing the root
	return render_template("403.html"), 403

@app.route('/<path:url>')
def process(url):
	r = request.path # r will be a string containing the path
	s = "./templates" + r # create full path to requested file

	# check for 403 codes
	if ('/~' in r) or ('/..' in r) or ('//' in r):
		return render_template("403.html"), 403
	else:
		try:
			f = open(s) # try and open the requested file
			return render_template(r) # if the open is successful we return the file
		except IOError: # file does not exist. 404 error
			return render_template("404.html"), 404


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
