# README #

### Author: Riley Matthews, rmatthe2@uoregon.edu ###

### What is this repository for? ###

This is a trivial webserver made using python and flask. Will serve valid GET .html and .css requests.

App files are located in the "web" folder. The "web/templates" subfolder contains .html and .css files.

### What do I need? ###

Docker is required in order to use the dockerfile.

* Build the simple flask app image using

  ```
  docker build -t UOCIS-flask-demo .
  ```
  
* Run the container using
  
  ```
  docker run -d -p 5000:5000 UOCIS-flask-demo
  ```